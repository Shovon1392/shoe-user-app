import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { SellerService } from '../services/sellerService/seller.service';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss'],
})
export class BaseComponent implements OnInit {

  constructor(
    public navCtrl : NavController,
    public activatedRoute : ActivatedRoute,


    /**CUStom services */
    public sellerService : SellerService,
  ) { }

  ngOnInit() {}
  noImage = 'https://thumbs.dreamstime.com/b/no-image-available-icon-flat-vector-no-image-available-icon-flat-vector-illustration-132482953.jpg';
  showLoader = true;
  /** go to previous page  */
  getBack(){
    this.navCtrl.back();
  }
  /**
   * Custom log
   */
  log(data){
    console.log(data);
  }

  /** get random images */
getRandomImages(images){

  //console.log(images)
  if(images.length > 0 ){
    // setInterval(function(){ 
      let randomNo = Math.floor(Math.random() * (images.length)); 
      console.log(randomNo)
      return images[randomNo]['thumb_image'];
    // }, 10000);
  } else {
    return this.noImage;
  }
}


/** GO to product details page */

getProductDetails(shopId,productId){
  this.navCtrl.navigateForward('stores/'+shopId+'/'+productId);
}


doRefresh(){
  this.ngOnInit()
}
/**End of class */
}
