import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SkeletonComponent } from './skeleton.component';

@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule],
  declarations: [SkeletonComponent],
  exports: [SkeletonComponent]
})
export class SkeletonModule { }
 