import { Component, Input, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';

@Component({
  selector: 'app-skeleton',
  templateUrl: './skeleton.component.html',
  styleUrls: ['./skeleton.component.scss'],
})
export class SkeletonComponent extends BaseComponent implements OnInit {

  @Input() loader_for: any;

  ngOnInit() {
    console.log(this.loader_for)
  }

}
