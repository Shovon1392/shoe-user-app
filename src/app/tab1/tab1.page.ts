import { Component } from '@angular/core';
import { BaseComponent } from '../base/base.component';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page extends BaseComponent {

  
  storeList : any = [];
  selectedLocation = 'durgapur';
  ngOnInit(): void {
    this.getStores(this.selectedLocation);
  }


  /** Get shop list by location */
  getStores(location){
    console.log(location)
    this.showLoader= true;
    // this.spinner.show();
    this.sellerService.getSellerByLocation(location).subscribe(
      res =>{
        // this.spinner.hide();
        this.storeList = res['data'][0];
        this.showLoader= false;
        console.log(res);
      }, err =>{
        // this.spinner.hide();
        console.log(err);
      }
    );
  }

  /** go to product list page*/
  getProductList(store){
    localStorage.setItem('lastShop', JSON.stringify(store));
    this.navCtrl.navigateForward('shop-products/'+store.shopId);
    // this.router.navigateByUrl('stores/'+store.shopId)
  }

  
  /**End of class */
}
