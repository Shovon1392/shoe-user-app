import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { timeout, catchError } from 'rxjs/operators';
import {RootService} from '../root.service';

@Injectable({
  providedIn: 'root'
})
export class UserService extends  RootService{

  constructor(http: HttpClient){
    super(http);
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
       //'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers
    };
  }

  /** get seller details by auth id */
  getUserById(id){
    return this.http.get(this.user_details+id,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    );
  }

  /** Add product to wish list */
  addProductToWishlist(data){
    return this.http.post(this.user_wishlist,data,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    );
  }

  getUserWishlists(id){
    return this.http.get(this.user_wishlist+'/'+id,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    );
  }

  /** End of class */
}
 